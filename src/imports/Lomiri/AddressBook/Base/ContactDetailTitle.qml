/*
 * Copyright (C) 2022 Ubports Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import Lomiri.Components.ListItems 1.3 as ListItems

Rectangle {
    id: root

    width: parent ? parent.width : undefined
    height: childrenRect.height
    property alias title: label.text
    color: theme.palette.normal.background

    Column {
        anchors {
            left: parent.left
            right: parent.right
            leftMargin: units.gu(2)
            rightMargin: units.gu(2)
        }

        Label {
            id: label
            height: units.gu(4)
            fontSize: "medium"
            verticalAlignment: Text.AlignVCenter
        }

        ListItems.ThinDivider {}
    }
}
