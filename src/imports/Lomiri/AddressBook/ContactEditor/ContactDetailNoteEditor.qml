/*
 * Copyright (C) 2022 UBports Foundation.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
import QtContacts 5.0 as QtContacts
import Lomiri.Contacts 0.1

import Lomiri.AddressBook.Base 0.1

//style needed for the TextArea
import Lomiri.Components.Themes.Ambiance 0.1

ContactDetailBase {
    id:root
    property string note
    property string originalValue
    property bool newNoteRequested: false

    detailType: QtContacts.ContactDetail.Note

    implicitHeight: contents.implicitHeight

    visible: !isEmpty() || newNoteRequested
    activeFocusOnTab: true

    signal newFieldAdded(QtObject field)

    onDetailChanged: {
        if (detail) {
            root.note = detail.note
            root.originalValue = root.note
        }
    }

    function isEmpty() {
        return input.text.length == 0 && (!root.originalValue || root.originalValue.length === 0)
    }

    function save() {
        var detailchanged = false
        if (!root.detail) {
            root.detail = root.contact.note
        }

        if (Qt.inputMethod) {
            Qt.inputMethod.commit();
        }

        if (originalValue.replace(/(\r\n|\n|\r)/gm, "") !== input.text.replace(/(\r\n|\n|\r)/gm, "")) {
            root.detail.note = input.text
            detailchanged = true
        }
        return detailchanged
    }

    Column {
        id: contents
        spacing: units.gu(1)
        width: parent.width

        ContactDetailTitle {
            id: detailTitle
            title: i18n.dtr("address-book-app", "Note")
        }

        TextArea {
            id: input
            property real textAreaHeight: lineCount * font.pixelSize
            anchors {
                margins: units.gu(2)
                left: parent.left
                right: parent.right
            }
            height: textAreaHeight < units.gu(8) ? units.gu(8) : Math.min(units.gu(26), textAreaHeight)

            text:  root.note
            persistentSelection: false
            wrapMode: TextEdit.WordWrap
            inputMethodHints: Qt.ImhNoPredictiveText
            placeholderText:i18n.dtr("address-book-app", "Write note")
            style: TextFieldStyle {
                overlaySpacing: 0
                frameSpacing: 0
                background: Item {}
            }

            // default style
            font {
                family: "Ubuntu"
                    pixelSize: activeFocus ? FontUtils.sizeToPixels("large") : FontUtils.sizeToPixels("medium")
            }

            onActiveFocusChanged: {
                if (activeFocus) {
                    idleMakeMeVisible(input)
                    deselect()
                    cursorPosition = text.length
                }
            }
        }
    }

    // when user is just swiping to the page, avoid the TextArea to become active
    MouseArea {
        enabled: !input.activeFocus
        anchors.fill: contents
        onClicked: input.forceActiveFocus()
    }

    Connections {
        target: addNewFieldButton
        onFieldSelected: {
            if (detailType === root.detailType) {
                newNoteRequested = true
                newFieldAdded(input)
            }
        }
    }
}
