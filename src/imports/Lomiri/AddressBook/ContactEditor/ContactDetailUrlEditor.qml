/*
 * Copyright (C) 2022 UBports Foundation.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
import QtContacts 5.0 as QtContacts
import Lomiri.Contacts 0.1

import Lomiri.AddressBook.Base 0.1

//style needed for the TextArea
import Lomiri.Components.Themes.Ambiance 0.1

ContactDetailBase {
    id:root
    property string url
    property string originalValue
    property bool newUrlRequested: false

    detailType: QtContacts.ContactDetail.Url

    implicitHeight: contents.height

    visible: !isEmpty() || newUrlRequested
    activeFocusOnTab: true

    signal newFieldAdded(QtObject field)

    onDetailChanged: {
        if (detail) {
            root.url = detail.url
            root.originalValue = root.url
        }
    }

    function isEmpty() {
        return input.text.length == 0 && (!root.originalValue || root.originalValue.length === 0)
    }

    function save() {
        var detailchanged = false
        if (!root.detail) {
            root.detail = root.contact.url
        }
        if (Qt.inputMethod) {
            Qt.inputMethod.commit();
        }

        if (originalValue !== input.text.trim()) {
            root.detail.setValue(QtContacts.Url, input.text.trim())
            detailchanged = true
        }
        return detailchanged
    }


    Column {
        id: contents
        spacing: units.gu(1)
        width: parent.width

        ContactDetailTitle {
            title: i18n.dtr("address-book-app", "Web address")
        }

        TextField {
            id: input
            text:  root.url

            anchors {
                margins: units.gu(2)
                left: parent.left
                right: parent.right
            }

            height: units.gu(4)

            placeholderText: i18n.dtr("address-book-app", "Type url")
            inputMethodHints: Qt.ImhUrlCharactersOnly
            style: TextFieldStyle {
                overlaySpacing: 0
                frameSpacing: 0
                background: Item {}
            }

            // default style
            font {
                family: "Ubuntu"
                pixelSize: activeFocus ? FontUtils.sizeToPixels("large") : FontUtils.sizeToPixels("medium")
            }

            onActiveFocusChanged: {
                if (activeFocus) {
                    idleMakeMeVisible(input)
                    cursorPosition = text.length
                }
            }
        }
    }

    Connections {
        target: addNewFieldButton
        onFieldSelected: {
            if (detailType === root.detailType) {
                newUrlRequested = true
                newFieldAdded(input)
            }
        }
    }
}
