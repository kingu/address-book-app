/*
 * Copyright (C) 2022 UBports Foundation.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
import QtContacts 5.0 as QtContacts
import Lomiri.Telephony.PhoneNumber 0.1

import Lomiri.AddressBook.Base 0.1

ContactDetailBase {
    id: root

    property string note : detail ? detail.note : ""

    detailType: QtContacts.ContactDetail.Note

    visible: implicitHeight > 0
    implicitHeight:(note.length > 0) ? contents.implicitHeight : 0

    function escapeHTML(text) {
        return text.replace(/[\t\n&><"' ]/g, function(m) {
            switch (m) {
            case '&':
                return '&amp;';
            case '<':
                return '&lt;';
            case '>':
                return '&gt;';
            case ' ':
                return '&nbsp;';
            case '"':
                return '&quot;';
            case '\n':
                return '<br>';
            case '\t':
                return '&nbsp;&nbsp;&nbsp;&nbsp;';
            default:
                return '&#039;';
            }
        });
    }

    function getCountryCode() {
        var localeName = Qt.locale().name
        return localeName.substr(localeName.length - 2, 2)
    }

    function parseText(text){
        if (!text) return ""

        const linkRegex = /([\w+]+\:\/\/)?([\w\d-]+\.)*[\w-]+[\.\:]\w+([\/\?\=\&\&amp;\#\.]?[\w-]+)*\/?/gm
        const region = getCountryCode()

        let lines = []
        text.split(/\n/).forEach( (line) => {
             if (line.length > 0) {

                 line = escapeHTML(line)

                 // links
                 line = line.replace(linkRegex, function(url) {
                     var hyperlink = url;
                     if (hyperlink.startsWith('www')) {
                         hyperlink = 'https://' + hyperlink;
                     }

                     return '<a href="' + hyperlink + '">' + url + '</a>';
                 })

                 // phone numbers
                 PhoneUtils.matchInText(line, region).forEach((phoneNumber) => {
                     line = line.replace(phoneNumber, '<a href="tel:///' + phoneNumber + '">' + phoneNumber + '</a>');
                 });
             }

             lines.push(line)
         })

        return lines.join('<br>')
    }

    Column {
        id: contents
        spacing: units.gu(1)
        width: parent.width

        ContactDetailTitle {
            title: i18n.dtr("address-book-app", "Note")
        }

        Rectangle {

            anchors {
                left: parent.left
                right: parent.right
                leftMargin: units.gu(2)
                rightMargin: units.gu(2)
            }
            height: childrenRect.height
            color: theme.palette.normal.background

            Text {
                id: label
                anchors.left: parent.left
                height: implicitHeight
                font.weight: Font.Light
                font.family: "Ubuntu"
                text: root.parseText(root.note)
                // It needs to be Text.StyledText to use linkColor: https://api-docs.ubports.com/sdk/apps/qml/QtQuick/Text.html#sdk-qtquick-text-linkcolor
                textFormat: Text.StyledText
                wrapMode: Text.Wrap
                onLinkActivated:  Qt.openUrlExternally(link)
            }
        }
    }
}
