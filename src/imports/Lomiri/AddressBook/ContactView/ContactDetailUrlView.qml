/*
 * Copyright (C) 2020 UBports Foundation.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
import QtContacts 5.0 as QtContacts

import Lomiri.AddressBook.Base 0.1


ContactDetailBase {
    id: root

    property string url : detail ? detail.url : ""

    detailType: QtContacts.ContactDetail.Url

    visible: implicitHeight > 0
    implicitHeight:(url.length > 0) ? contents.implicitHeight : 0

    Column {
        id: contents
        spacing: units.gu(1)
        width: parent.width

        ContactDetailTitle {
            title: i18n.dtr("address-book-app", "Web address")
        }

        Rectangle {

            anchors {
                left: parent.left
                right: parent.right
                leftMargin: units.gu(2)
                rightMargin: units.gu(2)
            }
            height: childrenRect.height
            color: theme.palette.normal.background

            Label {
                id: label
                anchors.left: parent.left
                anchors.right: icon.left
                height: units.gu(3)
                fontSize: "medium"
                text: root.url
                elide: Label.ElideRight
            }

            Icon {
                id: icon

                anchors.right: parent.right
                anchors.rightMargin: units.gu(1)
                width: units.gu(2.5)
                height: width
                name: "external-link"
                color: root.activeFocus ? theme.palette.normal.focus : theme.palette.normal.baseText
                asynchronous: true
            }
        }
    }

    onClicked: {
        Qt.openUrlExternally(root.url)
    }
}
