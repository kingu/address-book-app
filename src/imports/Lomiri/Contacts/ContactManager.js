var defaultManager = "galera"

function manager()
{
    return (typeof(QTCONTACTS_MANAGER_OVERRIDE) !== "undefined") &&
           (QTCONTACTS_MANAGER_OVERRIDE != "") ?
           QTCONTACTS_MANAGER_OVERRIDE : defaultManager
}
