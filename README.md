# Building for desktop

    mkdir build
    cd build
    cmake ..
    make

# Run

## on desktop

    cd build
    ./src/app/address-book-app

## the QML tests

    cd build
    make test or ctest 

## the Autopilot tests

    cd build
    make autopilot

# Building for click

To build for a click package:

    clickable

## i18n: Translating Lomiri's Address Book App into your Language

You can easily contribute to the localization of this project (i.e. the
translation into your language) by visiting (and signing up with) the
Hosted Weblate service:
https://hosted.weblate.org/projects/lomiri/address-book-app

The localization platform of this project is sponsored by Hosted Weblate
via their free hosting plan for Libre and Open Source Projects.
